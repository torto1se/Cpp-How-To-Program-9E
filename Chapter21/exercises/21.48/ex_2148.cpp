/*
 * =============================================================================
 *
 *       Filename:  ex_2148.cpp
 *
 *    Description:  Exercise 21.48 - Morse Code
 *
 *        Version:  1.0
 *        Created:  11/06/18 19:04:59
 *       Revision:  none
 *       Compiler:  g++
 *
 *         Author:  Siidney Watson - siidney.watson.work@gmail.com
 *   Organization:  LolaDog Studio
 *
 * =============================================================================
 */
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

const size_t AM_SZ = 36;    // size of alphanumeric and morse arrays

// alphanumeric chars
const char* ALNUM[AM_SZ] = {
    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
    "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
    "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"
};

// morse equivalents
const char* MORSE[AM_SZ] = {
    ".-",       // A
    "-...",     // B
    "-.-.",     // C
    "-..",      // D
    ".",        // E
    "..-.",     // F
    "--.",      // G
    ".....",    // H
    "..",       // I
    ".---",     // J
    "-.-",      // K
    ".-..",     // L
    "--",       // M
    "-.",       // N
    "---",      // O
    ".--.",     // P
    "--.-",     // Q
    ".-.",      // R
    "...",      // S
    "-",        // T
    "..-",      // U
    "..-",      // V
    ".--",      // W
    "-..-",     // X
    "-.--",     // Y
    "--..",     // Z
    ".----",    // 1
    "..---",    // 2
    "...--",    // 3
    "....-",    // 4
    ".....",    // 5
    "-....",    // 6
    "--...",    // 7
    "---..",    // 8
    "----.",    // 9
    "-----",    // 0
};

/**
 * Converts the given alphanumeric chars to its morse equivalent.
 * @param const char*
 */
void alnumToMorse(const char* base) {
    while (static_cast<bool>(*base)) {
        // space - 3 spaces between words (2 + space before letter)
        if (static_cast<bool>(*base == ' ')) {
            printf("  ");
        // 0 - 25 chars
        } else if (static_cast<bool>(isalpha(*base))) {
            for (int i = 0; i < 26; ++i) {
                if (toupper(*base) == *ALNUM[i]) {
                    printf("%s ", MORSE[i]);
                }
            }
        // 26 - 35 numbers
        } else if (static_cast<bool>(isdigit(*base))) {
            for (unsigned int i = 26; i < AM_SZ; ++i) {
                if (*base == *ALNUM[i]) {
                    printf("%s ", MORSE[i]);
                }
            }
        }
        ++base;
    }

    printf("\n");
}  // end method alnumToMorse

/**
 * Converts the given morse chars to its alphanumeric equivalent.
 * @param const char*
 */
void morseToAlnum(const char* base) {
    // tokenize string
    while (static_cast<bool>(*base)) {
        // largest morse letter is 5 chars
        char temp[6] = " ";

        // letter
        if (*base != ' ') {
            char* tempPtr = temp;

            // build temp string for decoding
            while (static_cast<bool>(*base) && *base != ' ') {
                *tempPtr = *base;

                ++base;
                ++tempPtr;
            }

            tempPtr = nullptr;

            // decode and print letter
            for (unsigned int i = 0; i < AM_SZ; ++i) {
                if (strcmp(temp, MORSE[i]) == 0) {
                    printf("%c", *ALNUM[i]);
                }
            }

        } else {
            // triple space between words
            if (*base == ' ' && *(base + 1) == ' ') {
                printf(" ");
                base += 2;
            } else {
                ++base;
            }
        }
    }

    printf("\n");
    // compare tokens
}  // end method morseToAlnum

/**
 * This is the main method.
 * @param argc.
 * @param argv[].
 * @return int.
 */
int main(int argc, char* argv[]) {
    const char* alnum[] = { "This is a test", "the 13th day", "another Test", "A frog in a pond" };

    const char* morse[] = { ".-   - . ... -   --- ..-.   .--. --- .-- . .-." };

    printf("\n----------\nALNUM TO MORSE\n----------\n");
    for (unsigned int i = 0; i < 4; ++i) {
        printf("%s\n", alnum[i]);
        alnumToMorse(alnum[i]);
    }

    printf("\n----------\nMORSE TO ALNUM\n----------\n");
    for (unsigned int i = 0; i < 1; ++i) {
        printf("%s\n", morse[i]);
        morseToAlnum(morse[i]);
    }


    return 0;
}  // end method main
